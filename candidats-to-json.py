#!/usr/bin/env python3

from openpyxl import load_workbook
import sqlite3
import pandas as pd
import json
import sys

fields = "nom prenom femme votants oui non commentaire_preselection commentaire_audition rang".split()


if __name__ == '__main__':
    filename = sys.argv[1]

    a = load_workbook(filename)

    # on prend le premier onglet
    w = a[a.sheetnames[0]]

    data = []

    R = range(1, 1 + w.max_column)

    def extract(i):
        return [ w.cell(i, j).value for j in R ]

    fields_in_sheet = extract(1)

    try:
        R = [ 1 + fields_in_sheet.index(f) for f in fields ]
    except ValueError as e:
        raise ValueError(f"Colonnes manquantes dans {filename}: {e}")

    i = 2

    while (L := extract(i))[0]:
        D = { fields[i]:L[i] for i in range(len(L)) }
        # some checks
        if int(D["oui"] or 0) + int(D["non"] or 0) != D["votants"]:
            raise ValueError("Erreur sur le nombre de votes:\n" +
                             json.dumps(D, indent=4))
        data.append(D)
        i += 1

    print(json.dumps(data, indent=4))
