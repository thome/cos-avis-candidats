
# remplissage automatique des avis candidats à partir d'un tableau excel

On trouve ici plusieurs scripts pour mécaniser quelques tâches
fastidieuses lors de l'organisation d'un comité de sélection.

Résultat des tests, et exemple de ce que ça produit (suivre le lien): [![pipeline status](https://gitlab.inria.fr/thome/cos-avis-candidats/badges/master/pipeline.svg)](https://gitlab.inria.fr/thome/cos-avis-candidats/-/jobs/artifacts/master/browse/output/?job=compile)

Rien n'est "universel", et l'esprit est que c'est à ajuster par concours,
par université, etc.

## prérequis

  - une machine capable de faire tourner du python3 et des scripts shell.
    Moi je fais ça sous linux. Je suppose que ça doit marcher sur mac
    aussi.

  - les templates de rapports voulus par la fac. Ici, on ne traite que de
    ce qui est le plus chronophage et qui mérite d'être automatisé, à
    savoir les avis motivés par candidats (plusieurs dizaines) et le PV
    de la première réunion.

  - un tableau excel qui résume les informations suivantes pour les
    candidats:
    - Nom et prénom
    - Femme ou non
    - Votes lors de la présélection (nombre de votants, de oui, de non)
    - Avis du jury lors de la présélection (qui devient l'"avis motivé")
 
    et, le cas échéant:
    - Avis du jury lors de l'audition (qui devient l'"avis circonstancié")
    - Rang de classement

  - un png de la signature du président du Cos

## installation des dépendances python

Il faut les packages python `pyyaml`, `python-docx`, `openpyxl`, et `pandas` (listés dans [`requirements.txt`](requirements.txt)).

Le mieux c'est d'utiliser un virtualenv, qui a l'avantage d'être jetable.
```
    python3 -m venv /tmp/venv
    /tmp/venv/bin/pip3 install -r requirements.txt
```

Mais si on est rétif aux virtualenv, on peut aussi faire `pip3 install -r
requirements.txt --user`.

## ajustement par concours

Éditer le fichier [`concours.yaml`](concours.yaml). Tous les champs sont
obligatoires.  Le champ `signature` est le nom d'un fichier au format png
qui doit contenir une signature à intégrer dans les documents.

Par défaut, les fichiers produits vont dans le répertoire courant, mais
il est aussi possible de les mettre ailleurs en ajustant
`output_directory`

## comment remplir le tableau

Les différentes infos doivent apparaître dans des colonnes du tableau
excel dont les noms sont nécessairement:
```
nom
prenom
femme
votants
oui
non
commentaire_preselection
commentaire_audition
rang
```

Le fichier [`candidats.xlsx`](candidats.xlsx) doit pouvoir servir
d'exemple. Pour les colonnes qui prennent des valeurs numériques ou
booléennes, ne rien mettre a le même effet que de mettre un 0.

## transformation en .json

La première étape ne sert pas à grand chose, si ce n'est à quitter très
vite le monde d'excel pour transporter les données vers un format normal.
```
/tmp/venv/bin/python3 candidats-to-json.py candidats.xlsx > candidats.json
```

(on peut aussi utiliser [`./doit.sh`](doit.sh))
    
## génération des avis et du PV de présélection

Si tout est installé comme il faut, il suffit de faire:

```
/tmp/venv/bin/python3 ./pv1.py
/tmp/venv/bin/python3 ./avis.py
```

## Licence

Domaine public. Faites ce que vous voulez avec. Contributions bienvenues.
