#!/usr/bin/env python3

import json
import yaml
import docx
import os
import re
from cos_candidats_utils import delete_thing, adjust_field

candidats = json.load(open("candidats.json", "r"))
concours = yaml.safe_load(open("concours.yaml", "r"))
template = "Annexe_1._PV_1ère_réunion.docx"

doc = docx.Document(template)

S = 0
S = adjust_field(doc, r"Nature.*emploi.*", concours["nature"], S)
S = adjust_field(doc, r".*section.*CNU", concours["section"], S)
S = adjust_field(doc, r"N° de l’emploi.*", f"{concours['code']} / Galaxie {concours['galaxie']}", S)
S = adjust_field(doc, r"Profil.*", concours["profil"], S)

T = doc.tables[0]

for i,candidat in enumerate(candidats):
    # If this assert fails, please extend the table from openoffice.
    # Apparently python-docx isn't good at adding rows.
    if 2+i >= len(T.rows):
        raise ValueError("Please modify the template so that the table has more empty rows")

    P = [ T.cell(2+i, j).paragraphs[0] for j in range(4) ]
    for p in P:
        p.clear()

    P[0].add_run('Mme' if candidat["femme"] else "M.")
    P[1].add_run(candidat["nom"])
    P[2].add_run(candidat["prenom"])
    if int(candidat["oui"] or 0) >= int(candidat["non"] or 0):
        P[3].add_run("Favorable")
    else:
        P[3].add_run("Défavorable")

while len(doc.tables[0].rows) > 2 + len(candidats):
    delete_thing(doc.tables[0].rows[-1])

# on s'ajuste sur l'endroit où il y a écrit "président"
S = adjust_field(doc, r".*président.*comité.*", '', S)
S = adjust_field(doc, r"Nom et Prénom.*", concours["president"], S)
S = adjust_field(doc, r"Date.*", concours["preselection"], S)
S = adjust_field(doc, r"Signature.*", concours["preselection"], S)
doc.paragraphs[S].runs[-1].add_picture(concours["signature"], width=docx.shared.Inches(1.25))

pwd = os.getcwd()
os.chdir(concours['output_directory'])
doc.save(f"{concours['code']}_PV_1ere_reunion.docx")
os.chdir(pwd)
