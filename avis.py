#!/usr/bin/env python3

import json
import yaml
import docx
import os
import re
from cos_candidats_utils import *

candidats = json.load(open("candidats.json", "r"))
concours = yaml.safe_load(open("concours.yaml", "r"))
template = "Annexe_4._Avis_sur_chaque_candidature.docx"


for i,candidat in enumerate(candidats):
    nom = candidat["nom"]
    prenom = candidat["prenom"]
    commentaire = candidat["commentaire_preselection"]

    shortlist = int(candidat["oui"] or 0) >= int(candidat["non"] or 0)
    admissible = int(candidat["rang"] or 0) > 0

    print(f"---- {i}: {nom} {prenom}")

    poste = concours['code']
    doc = docx.Document(template)

    S = 0

    S = adjust_field(doc, "Nom.*personne.*candidate", f"{nom} {prenom}", S)
    S = adjust_field(doc, r"Nature.*emploi.*", concours["nature"], S)
    S = adjust_field(doc, r".*section.*CNU", concours["section"], S)
    S = adjust_field(doc, r"N° de l’emploi.*", f"{concours['code']} / Galaxie {concours['galaxie']}", S)
    S = adjust_field(doc, r"Profil.*", concours["profil"], S)


    presents = int(concours['presents_preselection'] or 0)
    votants = int(candidat['votants'] or 0)
    non_votants = presents - votants

    T = doc.tables[0]
    lignes_tableau = [i.cells[0].text for i in T.rows]

    def find(r):
        for i,l in enumerate(lignes_tableau):
            if re.match(r, l):
                return i
        raise ValueError("Le tableau ne contient pas " + r)

    T.cell(find(r"Membres présent.*"), 0).add_paragraph(str(presents))
    T.cell(find(r"Nombre de votant.*"), 1).add_paragraph(str(votants))
    T.cell(find(r"Nombre de non votant.*"), 1).add_paragraph(str(non_votants))

    blabla = T.cell(find(r".*examen du dossier.*"),0).paragraphs[0]
    i = kill_run(blabla.runs, "xx/xx/")
    i = replace_run(blabla.runs, "xxxx", concours['preselection'], i)
    blabla.runs[i].font.highlight_color = None

    T.cell(find(r"Avis favorable.*"), 1).add_paragraph(str(int(candidat["oui"] or 0)))
    T.cell(find(r"Avis défavorable.*"), 1).add_paragraph(str(int(candidat["non"] or 0)))
    T.cell(find(r"Avis motivé.*"), 0).add_paragraph(commentaire)

    S = adjust_field(doc, r".*président.*comité.*", '', S)
    S = adjust_field(doc, r"Nom et Prénom.*", concours["president"], S)
    S = adjust_field(doc, r"Date.*", concours["preselection"], S)
    S = adjust_field(doc, r"Signature.*", '', S)
    doc.paragraphs[S].runs[-1].add_picture(concours["signature"], width=docx.shared.Inches(1.25))
    S += 1

    if not shortlist:
        while len(doc.paragraphs) > S:
            delete_thing(doc.paragraphs[S])
        delete_thing(doc.tables[1])
    else:
        T = doc.tables[1]
        lignes_tableau = [i.cells[0].text for i in T.rows]


        blabla = T.cell(find(r".*audition de la personne.*"),0).paragraphs[0]
        i = kill_run(blabla.runs, "xx/xx/")
        i = replace_run(blabla.runs, "xxxx", concours['auditions'], i)
        blabla.runs[i].font.highlight_color = None

        oui_class = admissible * int(concours['votants_admissibilite'])
        non_class = (1 - admissible) * int(concours['votants_admissibilite'])
        
        T.cell(find(r"Avis favorable.*"), 1).add_paragraph(str(oui_class))
        T.cell(find(r"Avis défavorable.*"), 1).add_paragraph(str(non_class))
        T.cell(find(r"Avis motivé.*"), 0).add_paragraph(candidat["commentaire_audition"])

        S = adjust_field(doc, r".*président.*comité.*", '', S)
        S = adjust_field(doc, r"Nom et Prénom.*", concours["president"], S)
        S = adjust_field(doc, r"Date.*", concours["auditions"], S)
        S = adjust_field(doc, r"Signature.*", '', S)
        doc.paragraphs[S].runs[-1].add_picture(concours["signature"], width=docx.shared.Inches(1.25))

    pwd = os.getcwd()
    os.chdir(concours['output_directory'])
    doc.save(f"{poste}_avis_{nom}_{prenom}.docx")
    os.chdir(pwd)

