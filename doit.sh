#!/bin/bash

if ! [ -d /tmp/venv ] ; then
	python3 -m venv /tmp/venv
fi

check_package() {
    import="$1"
    pip="$2"
    : ${pip:="$1"}
    if ! /tmp/venv/bin/python3 -c "import $import" 2>/dev/null ; then
            /tmp/venv/bin/pip3 install "$pip"
    fi
}

check_package docx python-docx
check_package openpyxl
check_package pandas
check_package yaml pyyaml

if [ candidats.xlsx -nt candidats.json ] ; then
    /tmp/venv/bin/python3 candidats-to-json.py candidats.xlsx > candidats.json.tmp && mv -f candidats.json.tmp candidats.json
fi

# ensuite c'est:
#
# /tmp/venv/bin/python3 ./make-letters.py
