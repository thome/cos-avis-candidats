import re

def delete_thing(thing):
    # https://github.com/python-openxml/python-docx/issues/33#issuecomment-77661907
    p = thing._element
    p.getparent().remove(p)
    p._p = p._element = None

def adjust_field(doc, regexp, data, start=0):
    while start < len(doc.paragraphs):
        c = doc.paragraphs[start]
        if re.match(regexp, c.text):
            sp = ' ' if c.text.endswith(':') else ''
            c.add_run(sp + str(data))
            return start
        start += 1
    raise ValueError(f"Le modèle ne semble pas contenir la regexp {regexp}")

def replace_run(thing, regexp, replacement, start=0):
    while start < len(thing):
        c = thing[start]
        if re.match(regexp, c.text):
            if replacement is not None:
                c.text = replacement
            else:
                delete_thing(c)
            return start
        start += 1
    raise ValueError(f"L'objet ne semble pas contenir la regexp {regexp}")

def kill_run(thing, regexp, start=0):
    return replace_run(thing, regexp, None, start)
